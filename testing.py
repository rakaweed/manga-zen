import requests
# from html.parser import HTMLParser
# import sqlite3
from bs4 import BeautifulSoup
import re
from urllib import parse

# class MyHTMLParser(HTMLParser):
#     def __init__(self):
#         super().__init__()
#         self.content = ""
#         self.current_index = 0
#         self.wanted_index = 0

#     def handle_starttag(self, tag, attrs):
#         # print("<", tag, ">", sep="")
#         # for attr in attrs:
#         #     print("     attr:", attr)
#         self.current_index += 1
#         if tag=='div' and ('id', 'mangaresults') in attrs:
#             print("here start tag", tag, attrs)
#             self.wanted_index = self.current_index
#             tag_attrs = ' '.join(["{}='{}'".format(attr, val) for attr, val in attrs])
#             # print('<{} {}>'.format(tag, tag_attrs))
#             self.content += '<{} {}>'.format(tag, tag_attrs)
#         else:
#             self.wanted_index = 0
#         # return {'tag': tag, 'attrs': attrs}

#     def handle_data(self, data):
#         if self.wanted_index==self.current_index:
#             print("here handling data", data)
#             self.content += str(data)

#     def handle_endtag(self, tag):
#         if self.wanted_index==self.current_index:
#             print("here end tag", tag)
#             self.content += '</{}>'.format(tag)
#         # print("</", tag, ">", sep="")
#         # return {'tag': tag}


# parser = MyHTMLParser()
base_url = "https://www.mangapanda.com"
try:
    # r = requests.get('https://www.mangapanda.com/')
    try:
        txt = open("outputs/response.html", "r")
        txt.close()
    except Exception as ex:
        print("Making Request...")
        goto_url = parse.urljoin(base_url, "/popular")
        r = requests.get(goto_url)
        print("Request Made")
        if r.status_code == 200:
            print("Response received:")
            txt = open("outputs\\response.html", "wt")
            txt.write(str(r.content))
            txt_pretty = open("outputs\\response_pretty.html", "wt")
            txt_pretty.write(BeautifulSoup(r.content, features="lxml").prettify())
            txt_pretty.close()
            txt.close()
        else:
            print("Request/Response Error:", r.status_code)

    with open("outputs\\response.html") as txt:
        the_content = BeautifulSoup(txt.read(), features="lxml")
        manga_search_list = the_content.find_all(attrs={"class": "mangaresultitem"})
        # manga_names = the_content.find_all(attrs={"class": "manga_name"})
        # for manga_name in manga_names:
        #     print(manga_name.find("a").text)
        manga = []
        for num, manga_container in enumerate(manga_search_list):
            # print("\n------manga------")
            # print(manga_container)
            # print("------manga------\n")
            image = manga_container.find(attrs={"class": "imgsearchresults"}).get("style")
            # image = re.search("(?P<url>https?://[^\s]+)", image).group("url")
            manga_name = manga_container.find(attrs={"class": "manga_name"}).find("a")
            # print("\nmanga_name")
            # print(manga_name)
            # print(manga_name.get("href"))
            name = manga_name.get_text().replace("\xa0", " ")
            link = parse.urljoin(base_url, manga_name.get("href"))
            author = manga_container.find(attrs={"class": "author_name"}).get_text().replace("\xa0", " ")
            chapters = manga_container.find(attrs={"class": "chapter_count"}).get_text().replace("\xa0", " ")
            genre = manga_container.find(attrs={"class": "manga_genre"}).get_text().replace("\xa0", " ")
            manga.append({
                "id": num,
                "name": name,
                "image": image,
                "link": link,
                "author": author,
                "genre": genre,
                "chapters": chapters
            })
        
        print(manga)
        txt.close()
except Exception as e:
    print("Some error occurred", e)