import requests
from bs4 import BeautifulSoup
import re
from urllib import parse
import json
import os
from zipfile import ZipFile
# from functools import reduce
from flask import Flask, make_response, send_file

app = Flask(__name__)

def extract_url(string):
    return re.search(r"(?P<url>https?://[^\s'\"]+)", string).group("url").rstrip("\\")


class MangaCrawler:
    def __init__(self, base_url):
        self.base_url = base_url

    def crawl(self, relative_url):
        goto_url = parse.urljoin(self.base_url, relative_url)
        r = requests.get(goto_url)
        if r.status_code == 200:
            return {'content': r.content, 'headers': r.headers}
        else:
            return None

@app.route('/')
def index():
    return popular()

@app.route('/mangapanda/popular')
def popular():
    try:
        manga_panda = MangaCrawler("https://www.mangapanda.com")
        manga_panda_popular = manga_panda.crawl("/popular")['content']
        the_content = BeautifulSoup(str(manga_panda_popular), features="lxml")
        manga_search_items = the_content.find_all(attrs={"class": "mangaresultitem"})
        manga_list = []
        for num, manga_container in enumerate(manga_search_items):
            image = extract_url(manga_container.find(attrs={"class": "imgsearchresults"}).get("style"))
            anchor_tag = manga_container.find(attrs={"class": "manga_name"}).find("a")
            name = anchor_tag.get_text().replace("\xa0", " ")
            # link = parse.urljoin(manga_panda.base_url, anchor_tag.get("href"))
            link = anchor_tag.get("href")
            author = manga_container.find(attrs={"class": "author_name"}).get_text().replace("\xa0", " ")
            chapters = manga_container.find(attrs={"class": "chapter_count"}).get_text().replace("\xa0", " ")
            genre = manga_container.find(attrs={"class": "manga_genre"}).get_text().replace("\xa0", " ")
            manga_list.append({
                "id": num,
                "name": name,
                "image": image,
                "link": link,
                "author": author,
                "genre": genre,
                "chapters": chapters
            })
        return json.dumps(manga_list)
    except Exception as e:
        print(e)
        return make_response(json.dumps({'message': 'Content not found.'}), 404)

# @app.route('/manga/<string:manga_name>')
# def manga_chapters(manga_name):
#     manga_panda = MangaCrawler("https://www.mangapanda.com")
#     manga_chapters = manga_panda.crawl(f"/{manga_name}")
#     the_content = BeautifulSoup(str(manga_chapters), features="lxml")
#     chapters = filter(None, map(lambda content: {"chapter_count": content.td.a.text, "chapter_name": content.td.a.next_sibling.replace(":","").strip(), "chapter_link": content.td.a.get("href")} if content.td else '', the_content.find(id="listing").find_all("tr")))
#     return json.dumps([chapter for chapter in chapters])

@app.route('/mangapanda/<string:manga_name>')
def manga_chapters(manga_name):
    try:
        manga_panda = MangaCrawler("https://www.mangapanda.com")
        manga_chapters = manga_panda.crawl(f"/{manga_name}")['content']
        the_content = BeautifulSoup(str(manga_chapters), features="lxml")
        chapters = the_content.find(id="listing").find_all("tr")
        chapters_list = []
        for num, chapter_container in enumerate(chapters):
            chapter_td = chapter_container.td
            if chapter_td:
                anchor_tag = chapter_td.a
                count = anchor_tag.text
                name = anchor_tag.next_sibling.replace(":","").strip()
                link = anchor_tag.get("href")
                chapters_list.append({
                    "id": num,
                    "chapter_count": count,
                    "chapter_name": name,
                    "chapter_link": link
                })
        return json.dumps(chapters_list)
    except Exception as e:
        print(e)
        return make_response(json.dumps({'message': 'Content not found.'}), 404)

@app.route('/mangapanda/<string:manga_name>/<int:chapter_num>')
def manga_chapter_pages(manga_name, chapter_num):
    try:
        manga_panda = MangaCrawler("https://www.mangapanda.com")
        manga_chapter_pages = manga_panda.crawl(f"/{manga_name}/{chapter_num}")['content']
        the_content = BeautifulSoup(str(manga_chapter_pages), features="lxml")
        pages = the_content.find(id="pageMenu").find_all("option")

        pages_list = []
        for page_num in range(1, len(pages)+1):
            page = manga_panda.crawl(f'{manga_name}/{chapter_num}/{page_num}')
            page_content = BeautifulSoup(str(page), features="lxml")
            page_img = page_content.find(id="img").get("src")
            pages_list.append(page_img)

        download_chapter_to_server(manga_name, chapter_num, pages_list)
        
        return send_file(f'outputs/{manga_name}_{chapter_num}.cbz', attachment_filename=f'{manga_name}_{chapter_num}.cbz', as_attachment=True)
    except Exception as e:
        print(e)
        return make_response(json.dumps({'message': 'Content not found.'}), 404)

def content_type_format(content_type):
    content_ext = {
        'image/jpeg': '.jpg',
        'image/jpg': '.jpg',
        'image/png': '.png',
    }
    return content_ext.get(content_type, '')

def download_chapter_to_server(manga_name, chapter_num, pages):
    downloaded_files = []
    os.makedirs(f'outputs/{manga_name}/{chapter_num}', exist_ok=True)
    with ZipFile(f'outputs/{manga_name}_{chapter_num}.cbz', mode='w') as manga_zip:
        os.chdir(f'outputs/{manga_name}/{chapter_num}')
        for num, image in enumerate(pages):
            image_crawler = MangaCrawler(image)
            image_file = image_crawler.crawl("")
            file_format = content_type_format(image_file['headers']['Content-Type'].lower())
            with open(f'{num:03d}{file_format}', 'wb') as img:
                img.write(image_file['content'])
                manga_zip.write(f'{num:03d}{file_format}')
            downloaded_files.append(f'outputs/{manga_name}/{chapter_num}/{num:03d}{file_format}')

    return downloaded_files