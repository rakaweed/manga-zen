# MangaZen

An app to view manga from multiple sources and download them to read offline.


## Installation:

### Using virtual environment

```
$ conda create -n mangazen-env python pip flask lxml beautifulsoup4 requests

$ conda activate mangazen-env
```

### Without using virtual environment

```
$ pip install flask lxml beautifulsoup4 requests
```

## Usage

### Setup proper environment variables

#### Windows:

```
$ set FLASK_APP=mangazen.py 
$ set FLASK_ENV=development
```

#### Unix:

```
$ export FLASK_APP=mangazen.py
$ export FLASK_ENV=development
```

### Run the application

```
$ flask run
```

## For VS Code Users

Use the following configuration for `launch.json`

```
{
    "name": "Flask",
    "type": "python",
    "request": "launch",
    "module": "flask",
    "env": {
        "FLASK_APP": "mangazen.py",
        "FLASK_ENV": "development",
        "FLASK_DEBUG": "0"
    },
    "args": [
        "run",
        "--no-debugger",
        "--no-reload"
    ],
    "jinja": true
}
```

Then start the application with `Start Debugging` button.